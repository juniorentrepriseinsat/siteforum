 const importantLinks = [
    {

        headerText: "Entreprendre à l’ère du Covid-19: naissance ou décadence?",
        link: "https://medium.com/@junior.entreprise.insat/entreprendre-%C3%A0-l%C3%A8re-du-covid-19-naissance-ou-d%C3%A9cadence-cbca2a509c15",
        imgURL : "https://miro.medium.com/max/3150/1*6_fgYnisCa9V21mymySIvA.png"

    },
    {

        headerText: "“L’envie d’entreprendre “ doit-elle être diffusée chez les étudiants ingénieurs?",
        link: "https://medium.com/@junior.entreprise.insat/lenvie-d-entreprendre-doit-elle-%C3%AAtre-diffus%C3%A9e-chez-les-%C3%A9tudiants-ing%C3%A9nieurs-bfbc9ee706f1",
        imgURL : "https://miro.medium.com/max/3150/1*6_fgYnisCa9V21mymySIvA.png"

    },
    
   
]




export default importantLinks