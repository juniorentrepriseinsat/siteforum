const actualities = [
    [
        {
            source: "instagram",
            url:"https://www.instagram.com/p/CHYZ7jVHYPk",
        },
        {
            source: "facebook",
            url: "https://www.facebook.com/junior.entreprise.insat/posts/3632030080163483"
        },
        {
            source: "instagram",
            url: "https://www.instagram.com/p/CF5HrJ6nqv3"
        },
        {
            source: "instagram",
            url: "https://www.instagram.com/p/CFegkqLHW-w"
        },
        {
            source: "facebook",
            url: "https://www.facebook.com/junior.entreprise.insat/posts/3625825290783962"
        },
        {
            source: "instagram",
            url: "https://www.instagram.com/p/CD34tw8HY1B"
        },
    ],
    [
        {
            source: "facebook",
            url:"https://www.facebook.com/junior.entreprise.insat/posts/3722320511134439"
        },
        {
            source: "instagram",
            url: "https://www.instagram.com/p/CHGY7dgnGwq"
        },
        {
            source: "facebook",
            url: "https://www.facebook.com/junior.entreprise.insat/posts/3719101421456348"
        },
        {
            source: "instagram",
            url: "https://www.instagram.com/p/CHGY5eendg5"
        },
        
    ],
    [
        
        {
            source: "facebook",
            url: "https://www.facebook.com/junior.entreprise.insat/posts/3716186598414497"
        },
        {
            source: "facebook",
            url: "https://www.facebook.com/junior.entreprise.insat/posts/3679515835414907"
        },
        {
            source: "facebook",
            url: "https://www.facebook.com/junior.entreprise.insat/posts/3634265363273288"
        },
        {
            source: "instagram",
            url: "https://www.instagram.com/p/CAJHNHoHvjp"
        },   
    ]
]


export default actualities;