import React, {useEffect} from 'react';
import {NavLink} from "react-router-dom";
import './navbar.css';
import { HashLink } from 'react-router-hash-link';

const Navbar = () => {

    useEffect(() => {
        const script = document.createElement('script');

        script.src = "./assets/js/main.js";
        script.async = true;

        document.body.appendChild(script);

        }
    );

    return (
        <>
            <nav id="nav">

                <ul>
                    <li><NavLink exact to="/"> Acceuil <div className="arrowHome"><span>&#8964;</span></div></NavLink>
                        <ul>
                            <li><HashLink to="/#FORUM" smooth>Présentation du Forum</HashLink></li>
                            <li><HashLink to="/#CHIFFRES_CLES" smooth>Chiffres clés</HashLink></li>
                            <li><HashLink to="/#TEAM" smooth>Présentation de l'équipe</HashLink></li>
                            <li><HashLink to="/#JEI" smooth>Présentation de la JEI</HashLink></li>
                            <li><HashLink to="/#PARTNERS" smooth>Nos partenaires</HashLink></li>
                            <li><HashLink to="/#footer" smooth>Gallerie</HashLink></li>
                        </ul>
                    </li>
                    <li><NavLink exact to="/actuality-page"> Actualités </NavLink></li>
                    <li><NavLink exact to="/sponsors-page"> Partenaires </NavLink></li>
                    <li><NavLink exact to="/previous-edition"> Ancienne Edition </NavLink></li>
                    <li><NavLink exact to="/contact-us"> Contactez-nous </NavLink></li>
                </ul>


            </nav>
        </>
    );

};

export default Navbar;