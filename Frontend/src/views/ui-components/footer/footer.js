import React, {useEffect, useState} from 'react';
import './footer.css';
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faFacebookF, faInstagram, faLinkedinIn} from "@fortawesome/free-brands-svg-icons";
import {faCamera} from "@fortawesome/free-solid-svg-icons";
import axios from '../../../helpers/axios';


const Footer = (props) => {
    const [galleryImages, setGalleryImages] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getImages();

    }, []);

    const getImages = () => {
        setLoading(true);
        axios.get('/gallery').then(resp => {
            if (resp.status === 200) {
                const images = resp.data.map(imgObj => imgObj.path);
                setGalleryImages(images);
            }
            setLoading(false);
        }).catch(error => {
            setLoading(false);
            console.log("ERROR", error);
        });
    };

    return (
        <div>
            <div id="footer">
                <div className="container">

                    {props.gallery &&

                    <div className="row">

                        <section className="col-12 col-12-mobile">
                            <header>
                                <h2 className="icon solid circled">
                                    <FontAwesomeIcon icon={faCamera}/>
                                    <span className="label">Photos</span>
                                </h2>
                            </header>
                            {galleryImages ?
                                <div className="row gtr-25 images ">
                                    {galleryImages.map((image, index) => {
                                        if (index < 4) {
                                            return (
                                                <div className="col-3 col-12-mobile" key={`gal-img-${index}`}>
                                                    <img className="image fit" style={{padding: '5px'}}
                                                         src={image}
                                                         alt="GalleryImage"/>
                                                </div>
                                            );
                                        } else return null;
                                    })
                                    }

                                </div> :
                                <p style={{color: "white", fontSize: "1.6em", textAlign: "center"}}>Loading...</p>}
                        </section>

                    </div>}

                    <hr/>
                    <div className="row">
                        <div className="col-12">

                            <section className="contact">

                                <div className="row">


                                    <div className="col-3 col-12-mobile">
                                        <div className="single-widget widget-quick-links">
                                            <header>
                                                <h3>Useful Links</h3>
                                            </header>
                                            <ul>
                                                <li>
                                                    <Link
                                                        to='/'><span>
                                                        > Acceuil</span>
                                                    </Link></li>
                                                <li><Link to='/actuality-page'><span>
                                                    > Actualités</span></Link></li>
                                                <li>
                                                    <Link
                                                        to='/sponsors-page'><span>
                                                        > Partenaires</span>
                                                    </Link></li>
                                                <li>
                                                    <Link
                                                        to='/previous-edition'><span>
                                                        > Ancienne Edition</span>
                                                    </Link></li>
                                                <li>
                                                    <Link
                                                        to='/contact-us'><span>
                                                        > Contactez-nous</span>
                                                    </Link></li>


                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-5 col-12-mobile">
                                        <header>
                                            <h3>Contact Us</h3>
                                        </header>

                                        <ul className="contact contact-list">
                                            <li className="icon brands fa-periscope">
                                                &nbsp;&nbsp;
                                                Untitled Inc<br/>
                                                1234 Somewhere Road Suite #2894<br/>
                                                Nashville, TN 00000-0000
                                            </li>
                                            <li className="icon brands fa-whatsapp ">&nbsp;&nbsp; (+216) 000-000-000
                                            </li>
                                            <li className="icon brands fa-telegram-plane">&nbsp;&nbsp; junior.entreprise.insat@gmail.com</li>

                                        </ul>


                                    </div>

                                    <div className="col-4 col-12-mobile">
                                        <header>
                                            <h3>Our location</h3>
                                        </header>

                                        <div className="mapouter">
                                            <div className="gmap_canvas">

                                                <iframe title="localisation-map" width="380" height="220"
                                                        id="gmap_canvas"
                                                        src="https://maps.google.com/maps?q=insat&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                                        frameBorder="0" scrolling="no" marginHeight="0"
                                                        marginWidth="0"></iframe>

                                            </div>

                                        </div>


                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <ul className="icons">

                                            <li><a target='_blank' href="https://facebook.com/junior.entreprise.insat/"
                                                   className="icon brands">
                                                <FontAwesomeIcon icon={faFacebookF}/>
                                                <span className="label"> Facebook </span>
                                            </a></li>
                                            <li><a target='_blank' href="https://instagram.com/insat_junior_entreprise/"
                                                   className="icon brands ">
                                                <FontAwesomeIcon icon={faInstagram}/>
                                                <span className="label">Instagram</span></a></li>

                                            <li><a target='_blank'
                                                   href="https://linkedin.com/company/junior-entreprise-insat/"
                                                   className="icon brands ">
                                                <FontAwesomeIcon icon={faLinkedinIn}/>
                                                <span className="label">Linkedin</span></a></li>
                                        </ul>
                                    </div>
                                </div>


                            </section>

                            <div className="copyright">
                                <ul className="menu">
                                    <li>&copy; All rights reserved.</li>
                                    <li>Junior Enterprise Insat</li>
                                </ul>
                            </div>


                        </div>

                    </div>
                </div>
            </div>

        </div>
    );

};

export default Footer;