import React from 'react';
import './go-up.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowUp} from "@fortawesome/free-solid-svg-icons";

//scroll to header of the page smoothly when the user clicks the arrow up button
const goUp = () => {
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    })
}

const GoUp = () => {
    return (
        <div>
            {/* the arrow up button to scroll up */}
            <FontAwesomeIcon icon={faArrowUp} className="goup" onClick={goUp}/>
        </div>
    );

};

export default GoUp;