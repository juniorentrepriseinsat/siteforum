import pic01 from "./images/pic01.jpg";

const JeiPresentation =()=> {
        return (
            <div id="JEI">
                <section id="banner">
                    <header id="presentationHeader">
                        <div className="presentationJeiLeft">
                            <a href="#"><img className="presentationPicture" src={pic01} alt=""/></a>
                        </div>
                        <div className="presentationJeiRight">
                            <h3 id="presentationTitle">Présentation de la <strong>Junior Entreprise INSAT</strong>.</h3>
                            <p id="presentationText">
                                Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum
                                Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum
                                Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum
                                Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum
                                Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum
                            </p>
                        </div>
                    </header>
                </section>
            </div>
        );
}

export default JeiPresentation;