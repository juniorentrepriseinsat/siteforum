import "./assets/css/partenaires.scss";
import React, { useState, useEffect } from "react";
import axios from "../../helpers/axios"

const Partners = () => {

        const [ partners, setPartners ] = useState([]);

        useEffect(() => {
            async function fetchPartners() {
                const result = await axios.get("sponsors");
                setPartners(p => [...p, ...result.data]);
            }

            fetchPartners();
        }, []);



        const renderPartnersLogos = partners.map((partner, index) => {
            return (
                <div className="slide">
                    <img src={partner.link} height="100" width="250" alt="" key={index}/>
                </div>
            );
        })

        return (
            <div id="PARTNERS">
                <div className="slider">
                    <div className="slide-track">
                        {renderPartnersLogos}
                    </div>
                </div>
                <br/>
                <div id="morePartnersDiv">
                    <a href="/sponsors-page" className="button">Voir tous les partenaires</a>
                </div>
            </div>
        );
}

export default Partners;