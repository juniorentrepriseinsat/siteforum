import CountUp from "react-countup";
import "./assets/css/statistics.css";
import Ticker from "./Ticker";
import entreprise from "./images/entreprise.png";
import debat from "./images/debat.png";
import participant from "./images/participant.png";
import workshop from "./images/workshop.png";

const Statistics =()=> {
        return (
            <div id="CHIFFRES_CLES">
                    <div className="StatsSection">
                        <div className="row aln-center">
                            <div className="Stat">
                                <section className="Box Style1">
                                    <img className="ImageStat" src={entreprise}/>
                                    <h4>
                                        <Ticker className="count" end={24} />Entreprises</h4>
                                </section>
                            </div>
                            <div className="Stat">
                                <section className="Box Style1">
                                    <img className="ImageStat" src={debat}/>
                                    <h4 style={{"white-space":"nowrap","margin-left":"-0.5em"}}><Ticker className="count" end={500} suffix="+"/> Participants aux débats</h4>
                                </section>
                            </div>
                            <div className="Stat">
                                <section className="Box Style1">
                                    <img className="ImageStat" src={participant}/>
                                    <h4><Ticker className="count" end={1500} suffix="+"/> Présents au Forum</h4>
                                </section>
                            </div>
                            <div className="Stat">
                                <section className="Box Style1">
                                    <img className="ImageStat" src={workshop}/>
                                    <h4><Ticker className="count" end={4} /> Workshops réussis</h4>
                                </section>
                            </div>
                        </div>
                    </div>
            </div>
        );
}

export default Statistics;