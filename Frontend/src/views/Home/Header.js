import React, {Component, useEffect, useRef, useState} from 'react';

const Header =()=>{

    //Countdown

    const [timerDays,setTimerDays]=useState("00");
    const [timerHours,setTimerHours]=useState("00");
    const [timerMinutes,setTimerMinutes]=useState("00");
    const [timerSeconds,setTimerSeconds]=useState("00");


    let interval=useRef();
    const startTimer=()=>{
        const countDownDate= new Date("December 09, 2020 08:00:00")
        interval= setInterval(()=>{
            const now=new Date().getTime();
            const distance= countDownDate - now;

            const days= Math.floor(distance/(1000*60*60*24));
            const hours=Math.floor(distance%(1000*60*60*24)/(1000*60*60));
            const minutes=Math.floor(distance%(1000*60*60)/(1000*60));
            const seconds=Math.floor(distance%(1000*60)/1000)

            if(interval<0) {
                //stop timer
                clearInterval(interval.current)
            }
            else{
                //update timer
                setTimerDays(days);
                setTimerHours(hours);
                setTimerMinutes(minutes);
                setTimerSeconds(seconds);
            }
        },1000);
    }

    useEffect(()=>{
        startTimer();
        return()=>{
            clearInterval(interval.current);
        };
    });

    const zeroPad =(num) =>{
        return num.toString().padStart(2, "0");
    }


    return (
            <div>
                <div id="homeHeader" className="inner">
                    <header>
                        <div className="row">
                            <div className="col-3">
                                <h1 id="countdown"><a href="#" id="logo">{zeroPad(timerDays)}:</a></h1>
                                <h6 id="countdownDetails"><a href="#" id="logo">DAYS</a></h6>
                            </div>
                            <div className="col-3">
                                <h1 id="countdown"><a href="#" id="logo">{zeroPad(timerHours)}:</a></h1>
                                <h6 id="countdownDetails"><a href="#" id="logo">HOURS</a></h6>
                            </div>
                            <div className="col-3">
                                <h1 id="countdown"><a href="#" id="logo">{zeroPad(timerMinutes)}:</a></h1>
                                <h6 id="countdownDetails"><a href="#" id="logo">MINUTES</a></h6>
                            </div>
                            <div className="col-3">
                                <h1 id="countdown"><a href="#" id="logo">{zeroPad(timerSeconds)}</a></h1>
                                <h6 id="countdownDetails"><a href="#" id="logo">SECONDS</a></h6>
                            </div>

                        </div>
                        <hr />
                        <p>Another fine freebie by HTML5 UP</p>
                    </header>
                    <footer>
                        <a href="#banner"className="button circled scrolly">Découvrir</a>
                    </footer>
                </div>
            </div>
        );
}

export default Header;