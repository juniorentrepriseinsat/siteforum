import React, {useEffect, useState} from 'react';
import axios from '../../helpers/axios';

const TeamPresentation =()=> {

        const [team, setTeam ] = useState([]);

        useEffect(() => {
            async function fetchTeam(){
                const result = await axios.get("octeam");
                setTeam(t => [...t, ...result.data]);
            }
            fetchTeam();
        }, []);


        const renderTeam = team.map((member, index) => {
            return (
                <article key={index}>
                    <a href="#" className="image featured"><img src={member.image_path} alt=""/></a>
                    <header>
                        <h3><a href="#">{member.name}</a></h3>
                    </header>
                    <p>{member.role}</p>
                </article>
            )
        })

        return (
            <div id="TEAM">
                <section className="carousel">
                    <div className="reel">
                        {renderTeam}
                    </div>
                </section>
            </div>
        );

}

export default TeamPresentation;