import React, {useState,useRef,useEffect} from 'react';
import "./assets/css/noscript.css";
import "./assets/css/main.css";
import "./assets/css/HomeStyle.css";
import JeiPresentation from "./JeiPresentation";
import ForumPresentation from "./ForumPresentation";
import TeamPresentation from "./TeamPresentation";
import Statistics from "./Statistics";
import Header from "./Header";
import Partners from "./Partners";
import useScript from "./UseScript";
import Footer from "../ui-components/footer/footer";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowUp} from "@fortawesome/free-solid-svg-icons";
import GoUp from "../ui-components/go-up/go-up";


const HomePage = () => {
    useEffect(()=>{
        window.scrollTo(0,0);
    },[]);

//Reloading scripts
    /*useScript('./assets/js/main.js');
    useScript('./assets/js/util.js');
    useScript('./assets/js/breakpoints.min.js');
    useScript('./assets/js/browser.min.js');
    useScript('./assets/js/jquery.scrollex.min.js');
    useScript('./assets/js/jquery.scrolly.min.js');
    useScript('./assets/js/jquery.dropotron.min.js');
    useScript('./assets/js/jquery.min.js');*/

        return (
            <React.Fragment>
                <div className="homepage is-preload">
                    <div id="page-wrapper">

                        {/*Header*/}
                        <div id="header">

                            {/*Inner*/}
                            <Header/>
                        </div>

                        {/*Jei presentation*/}
                        <ForumPresentation/>
                        <br/>

                        {/*Chiffres clés*/}
                        <Statistics/>

                        {/*Carousel*/}
                        <TeamPresentation/>
                        <br/>

                        {/*Forum presentation*/}
                        <JeiPresentation/>
                        <br/>

                        {/*Partenaires*/}
                       <Partners/>
                       <br/>
                       <Footer gallery />

                    </div>
                    <GoUp/>
                </div>
            </React.Fragment>
        );
}

export default HomePage;