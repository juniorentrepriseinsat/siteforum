import pic01 from "./images/pic01.jpg";

const ForumPresentation = () =>{
        return (
            <div id="FORUM">
                <section id="banner">
                    <header id="reverse">
                        <div className="presentationForumLeft">
                            <h3 id="presentationTitle">Présentation du <strong>Forum</strong>.</h3>
                            <p id="presentationText">
                                Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum
                                Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum
                                Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum
                                Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum
                                Lorem ipsum lorem ipsum lorem ipsum Lorem ipsum lorem ipsum lorem ipsum
                            </p>
                            <br/>
                        </div>
                        <div className="presentationForumRight">
                            <a href="#"><img className="presentationPicture" src={pic01} alt=""/></a>
                        </div>
                    </header>
                </section>
            </div>
        );
}

export default ForumPresentation;