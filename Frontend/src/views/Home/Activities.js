import React, {Component} from 'react';
import pic07 from "./images/pic07.jpg";
import pic08 from "./images/pic08.jpg";
import pic09 from "./images/pic09.jpg";

const Activities =()=> {
        return (
            <div>
                <div className="wrapper style1">

                    <section id="features" className="container special">
                        <header>
                            <h2>Morbi ullamcorper et varius leo lacus</h2>
                            <p>Ipsum volutpat consectetur orci metus consequat imperdiet duis integer semper magna.</p>
                        </header>
                        <div className="row">
                            <article className="col-4 col-12-mobile special">
                                <a href="#" className="image featured"><img src={pic07} alt="" /></a>
                                <header>
                                    <h3><a href="#">Stands</a></h3>
                                </header>
                                <p>
                                    Amet nullam fringilla nibh nulla convallis tique ante proin sociis accumsan lobortis. Auctor etiam
                                    porttitor phasellus tempus cubilia ultrices tempor sagittis. Nisl fermentum consequat integer interdum.
                                </p>
                            </article>
                            <article className="col-4 col-12-mobile special">
                                <a href="#" className="image featured"><img src={pic08} alt="" /></a>
                                <header>
                                    <h3><a href="#">Workshops</a></h3>
                                </header>
                                <p>
                                    Amet nullam fringilla nibh nulla convallis tique ante proin sociis accumsan lobortis. Auctor etiam
                                    porttitor phasellus tempus cubilia ultrices tempor sagittis. Nisl fermentum consequat integer interdum.
                                </p>
                            </article>
                            <article className="col-4 col-12-mobile special">
                                <a href="#" className="image featured"><img src={pic09} alt="" /></a>
                                <header>
                                    <h3><a href="#">Débats</a></h3>
                                </header>
                                <p>
                                    Amet nullam fringilla nibh nulla convallis tique ante proin sociis accumsan lobortis. Auctor etiam
                                    porttitor phasellus tempus cubilia ultrices tempor sagittis. Nisl fermentum consequat integer interdum.
                                </p>
                            </article>
                        </div>
                    </section>

                </div>

            </div>
        );
}

export default Activities;