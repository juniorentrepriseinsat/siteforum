import React, {useEffect, useState} from 'react';
import Timeline from "./Timeline";
import Footer from "../ui-components/footer/footer";
import GoUp from "../ui-components/go-up/go-up";
import axios from "../../helpers/axios";

const PreviousEdition =() =>{

    const [fetchedTimeline,setFetchedTimeline] = useState(null);

    useEffect(()=>{
        window.scrollTo(0,0);
        fetchTimeline();
    },[]);

    const fetchTimeline = () => {
        axios.get("timeline").then(resp => setFetchedTimeline(resp.data)).catch(err => {
           alert(err);
           console.log("ERROR",err);
        });
    };

    return (
        <div id="page-wrapper">
            {fetchedTimeline &&
            <Timeline data={fetchedTimeline}/>}
            <GoUp/>
            <Footer />
        </div>
    );
}

export default PreviousEdition;