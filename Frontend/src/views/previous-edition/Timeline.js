import React, {useEffect} from 'react';
import $ from "jquery";
import jQuery from 'jquery';
import './timeline.css';


function Timeline(props) {
    useEffect(() => {
        (function ($) {
            $.fn.timeline = function () {
                $(".timeline-container").animate({scrollTop: 0}, "slow");
                var selectors = {
                    id: $(this),
                    item: $(this).find(".timeline-item"),
                    activeClass: "timeline-item--active",
                    img: ".timeline__img"
                };
                selectors.item.eq(0).addClass(selectors.activeClass);
                selectors.id.css(
                    "background-image",
                    "url(" +
                    selectors.item
                        .first()
                        .find(selectors.img)
                        .attr("src") +
                    ")"
                );
                var itemLength = selectors.item.length;
                $(window).scroll(function () {
                    var max, min;
                    var pos = $(this).scrollTop();
                    selectors.item.each(function (i) {
                        min = $(this).offset().top - 150;
                        max = $(this).height() + $(this).offset().top;
                        if (i === itemLength - 1 && pos > min + $(this).height() / 2) {
                            selectors.item.removeClass(selectors.activeClass);
                            selectors.id.css(
                                "background-image",
                                "url(" +
                                selectors.item
                                    .last()
                                    .find(selectors.img)
                                    .attr("src") +
                                ")"
                            );

                            selectors.item.last().addClass(selectors.activeClass);
                        } else if (pos <= max - 40 && pos >= min) {
                            selectors.id.css(
                                "background-image",
                                "url(" +
                                $(this)
                                    .find(selectors.img)
                                    .attr("src") +
                                ")"
                            );

                            selectors.item.removeClass(selectors.activeClass);
                            $(this).addClass(selectors.activeClass);
                        }
                    });
                });
            };
        })(jQuery);
        $("#timeline-1").timeline();
        // return () => window.removeEventListener('scroll' , $("#timeline-1").timeline());
    }, []);

    const events = props.data;

    return (

        <div className="timeline-container" id="timeline-1">
            <div className="timeline-header">
                <h2 className="timeline-header__title">JEI Forum</h2>
                <h3 className="timeline-header__subtitle">Previous Edition</h3>
            </div>

            <div className="timeline">
                {events.map((event) => (

                    <div className="timeline-item" data-text={event.element_time} key={event.element_time+"-"+event.title}>

                        <div className="timeline__content">
                            <img alt="event_img" className="timeline__img"
                                 src={event.image_path}/>
                            <h2 className="timeline__content-title">{event.title}</h2>
                            <p className="timeline__content-desc">{event.description}</p>
                        </div>

                    </div>

                ))}
            </div>
        </div>
    );
}

export default Timeline;
