import React,{Component} from 'react';
import './contact_us.css';
import Footer from "../ui-components/footer/footer";
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import emailjs from 'emailjs-com';
import GoUp from "../ui-components/go-up/go-up";


const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


class ContactUs extends Component{

    state={
        name:'',
        email : '',
        message : '',
        openSnack : false
    }

    componentDidMount() {
        window.scrollTo(0,0);
    }

    handleChange = (event,field) =>{
        this.setState({[field]:event.target.value});
    }

    handleSubmit = (event)=>{
        event.preventDefault();

        emailjs.sendForm('service_wzuz4oa', 'template_zvxyxhd', event.target, 'user_vCagOuIoRtYenLoK48Nqk')
            .then((result) => {
                if(result.status===200)
                this.setState({
                    name:'',
                    email : '',
                    message : '',
                    openSnack : true
                });

            }, (error) => {
                alert(error.text)
                console.log(error.text);
            });




    }
    handleSnackClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({openSnack : false});
    };


    render() {
        const {name,email,message,openSnack} = this.state;
        const {handleSnackClose} = this;

        return(
            <div id="page-wrapper">
                <Snackbar
                    anchorOrigin={{vertical : 'bottom',horizontal: 'right' }}
                    open={openSnack} autoHideDuration={3000} onClose={handleSnackClose}>
                    <Alert onClose={handleSnackClose} severity="success">
                        Votre Message a été bien envoyé ! Merci bien !
                    </Alert>
                </Snackbar>


            <section id="contactUs">
            <div className="inner">
                <h2 className="major">Nous Contacter</h2>
                <p>Priere de remplir Ce formulaire afin que nous pouvons vous contacter</p>
                <form onSubmit={this.handleSubmit}>
                    <div className="fields">
                        <div className="field">
                            <label htmlFor="name">Name</label>
                            <input type="text" onChange={(event) => this.handleChange(event,'name')} name="name" id="name" value={name} required/>
                        </div>
                        <div className="field">
                            <label htmlFor="email">Email</label>
                            <input value={email} type="email" onChange={(event) => this.handleChange(event,'email')} name="email" id="email" required />
                        </div>
                        <div className="field">
                            <label htmlFor="message">Message</label>
                            <textarea value={message} name="message" onChange={(event) => this.handleChange(event,'message')} id="message" rows="4" required> </textarea>
                        </div>
                    </div>
                    <ul className="actions">
                        <li><input type="submit" value="Send Message"  /></li>
                    </ul>
                </form>
                <ul className="contact">
                    <li className="icon brands fa-periscope">

                        Untitled Inc<br />
                        1234 Somewhere Road Suite #2894<br />
                        Nashville, TN 00000-0000
                    </li>
                    <li className="icon brands fa-whatsapp "> (+216) 000-000-000</li>
                    <li className="icon brands fa-telegram-plane"><a href="#"> junior.entreprise.insat@gmail.com</a></li>
                    <li className="icon brands fa-instagram"><a target='_blank' href="https://instagram.com/insat_junior_entreprise/">instagram.com/insat_junior_entreprise</a></li>
                    <li className="icon brands fa-facebook-f"><a target='_blank' href="https://facebook.com/junior.entreprise.insat/">facebook.com/junior.entreprise.insat</a></li>
                    <li className="icon brands fa-linkedin-in"><a target='_blank' href="https://linkedin.com/company/junior-entreprise-insat/">linkedin.com/company/junior-entreprise-insat</a></li>
                </ul>
               {/* <ul className="copyright">
                    <li>&copy; Untitled Inc. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>*/}
                <GoUp/>
            </div>
            </section>
                <Footer gallery />

            </div>
        );
    }
}

export default ContactUs;
