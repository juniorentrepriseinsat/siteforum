import React, { useState, useEffect } from "react";
import InstagramEmbed from "react-instagram-embed";
import { InView } from "react-intersection-observer";
import axios from "../../helpers/axios";
import Footer from "../ui-components/footer/footer";

import "../../assets/main.css"
import GoUp from "../ui-components/go-up/go-up";

const ActualityPage = () => {

    useEffect(()=>{
        window.scrollTo(0,0);
    },[]);


    //this section handles the infinite scroll
    const [ loading, setLoading ] = useState(false);
    const [ nbPage, setnbPage] = useState(1);
    const [ hasMore, sethasMore ] = useState(true);
    const [ feed, setFeed ] = useState([]);
    const [ articles, setArticles ] = useState([]);

   

    



    useEffect(() => {
        async function loadFeed() {
            setLoading(true);
            const result = await axios.get(`news?page=${nbPage}`);
            setFeed(f => [...f, ...result.data.news])
            sethasMore(nbPage < result.data.totalPages);
            setLoading(false);
        }

        async function loadArticles() {
            const result = await axios.get('article');
            setArticles(a => [...a, result.data]);
        }

        loadArticles();
        loadFeed();
    }
    , [nbPage]);

    

    //Render social media iframes from actualities information 
    const renderActualities = feed.map((act, index) => {
        
        if(act.source !== "facebook")
        return (
            <div className="col-10 col-12-mobile"  key={index} style={{margin: "0 auto"}}><InstagramEmbed url={act.link} clientAccessToken="2755158108029951|d6b00286d5afd0ba9c23c5553d7a1aee" key={index} style={{margin: "0 auto"}} allowFullScreen="" title="Embedded post" className="col-10 col-12-mobile" scrolling="no" frameBorder="0"></InstagramEmbed></div>
        )
    
        return <div className="fb-post col-10 col-12-mobile" key={index} style={{margin: "0 auto"}}  data-href={act.link}></div>
    
    });



    //Render important links 
    const renderArticles = articles.map((art, index) => {
        return (
        <div className="row" key={index}>    
            <div className="col-4">
                <img src={art.image_path} className="image fit" alt="" />
            </div>
            <div className="col-8" style={{margin: "auto"}}>
                <a href={art.link} target="blank"><h4>{art.title}</h4></a>
            </div>
        </div>    
        )
    })

    /*const fetchData = () => {
        console.log(hasMore);
        setFeed([...feed, ...actualities[nbPage]]);
        setnbPage(prevPageNb => prevPageNb+1);
    }*/


    return (
        <div id="page-wrapper">
            <div id="actualities" className="wrapper style1">
                {/* the main page */}
                <div className="container">
                    <div className="row gtr-200">
                        {/* Sidebar */}
                        <div className={`col-4 col-12-mobile imp-mobile `} id="sidebar">
                            <section>
                                <header style={{marginBottom: "30px"}}>
                                    <h3>Des lien importants</h3>
                                </header>

                                <div className="row gtr-50">
                                   {renderArticles}
                                </div>

                            </section>
                        </div>
                        {/* Main content */}
                        <div className="col-8 col-12-mobile" id="content">
                            <article id="main" >
                                <header style={{marginBottom: "50px", textAlign: "center"}}>
                                    <h2>Les Actualités</h2>
                                </header>
                                <div className="row">
                                    {renderActualities}
                                </div>
                            </article>
                            <InView onChange={(inView) => {if(inView && hasMore && !loading) setnbPage(prevPageNb => prevPageNb+1);} }>
                                {loading && <p className="articles-end">Loading ...</p>}
                                {!hasMore && !loading && <p className="articles-end">No more articles to show</p>}
                            </InView>

                        </div>
                    </div>
                    <GoUp/>
                </div>
            </div>
            <Footer gallery />
        </div>
    );
}

export default ActualityPage;