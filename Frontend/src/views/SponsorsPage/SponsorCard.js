import React, { useState } from "react";
import ReactCardFlip from "react-card-flip";

import "../../assets/main.css"

const SponsorsPage = (props) => {

    //the state of the card (flipped or not)
    const [ isFlipped, setIsFlipped ] = useState(false);

    //handles flipping the card when user clicks on it 
    const handleFlip = () => {
        setIsFlipped(!isFlipped);
    }

    const handleEnter = () => {
        setIsFlipped(true);
    }

    const handleLeave = () => {
        setIsFlipped(false);
    }


    return (
        <div className={props.className + " center-mobile"} onMouseEnter={handleEnter} onMouseLeave={handleLeave} onClick={handleFlip}>
            <ReactCardFlip isFlipped={isFlipped} flipSpeedBackToFront="1" flipSpeedFrontToBack="1" flipDirection="horizontal">
                    {/* card front face */}
                    <article style={{height: "256px"}}>
                        <img className="image fit" src={`${props.logo_path}`} alt=""/>
                    </article>
                    {/* card back face */}
                    <article className=" special" style={{height: "256px"}}>
                        <header>
                            <h3>{props.name}</h3>
                        </header>
                        <p>{props.domain}</p>
                    </article>
            </ReactCardFlip>
        </div>
    );
}

export default SponsorsPage;