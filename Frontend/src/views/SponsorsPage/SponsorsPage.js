import React, { useEffect, useState } from "react";
import axios from "../../helpers/axios"
import SponsorCard from "./SponsorCard"

import "../../assets/main.css"
import Footer from "../ui-components/footer/footer";
import GoUp from "../ui-components/go-up/go-up";

const SponsorsPage = () => {
    useEffect(()=>{
        window.scrollTo(0,0);
    },[]);

    const [sponsors, setSponsors] = useState([]);

    useEffect(() => {
        async function fetchData() {
            const result = await axios.get('sponsors');
            setSponsors(s => [...s, ...result.data]);
            
        }
        fetchData();
    }, []);

    //Create sponsor cards from sponsors informations
    const renderSponsors = (pack, className) => {
        return sponsors.filter(sponsor => sponsor.pack === pack).map((sponsor, index) => {
            return (
                <SponsorCard className={className} logo_path={sponsor.logo_path} name={sponsor.name} domain={sponsor.domain} key={index}/>
            )
        })
    }


    return (
        <div id="page-wrapper">
            <div className="wrapper style 1" id="sponsors">
                <section id="features" className="container special">
                    {/* main header */}
                    <header>
                        <h2>Nos Sponsores</h2>
                        <p>Ipsum volutpat consectetur orci metus consequat imperdiet duis integer semper magna.</p>
                    </header>
                    {/* sponsors cards */}
                    <div className="row">
                        {renderSponsors("1", "col-6 col-12-mobile")}
                    </div>
                    
                    <hr/>      
                    
                    <div className="row">
                        {renderSponsors("2", "col-4 col-10-mobile")}
                    </div>
                    
                    <hr/>

                    <div className="row">
                        {renderSponsors("3", "col-3 col-8-mobile")}
                    </div>
                </section>
                <GoUp/>
            </div>
            <Footer />
        </div>
    );
}

export default SponsorsPage;