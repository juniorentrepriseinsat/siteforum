import "./App.css";
import React from "react";
import Navbar from "./views/ui-components/navbar/navbar";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import ContactUs from "./views/contact-us/contact_us";
import PreviousEdition from "./views/previous-edition/previous_edition";
import HomePage from "./views/Home/HomePage";
import ActualityPage from "./views/ActualityPage/ActualityPage";
import SponsorsPage from "./views/SponsorsPage/SponsorsPage";


function App() {
    return (
          <Router>
                <Navbar/>
                <Switch>
                    <Route exact path="/" component={HomePage}/>
                    <Route exact path="/actuality-page" component={ActualityPage}/>
                    <Route exact path="/sponsors-page" component={SponsorsPage}/>
                    <Route exact path="/previous-edition" component={PreviousEdition}/>
                    <Route exact path="/contact-us" component={ContactUs}/>
                </Switch>
            </Router>

    );
}

export default App;
