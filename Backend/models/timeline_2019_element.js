'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Timeline_2019_element extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Timeline_2019_element.init({
    image_path: DataTypes.STRING,
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    element_time: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Timeline_2019_element',
    timestamps: false
  });
  return Timeline_2019_element;
};