'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OC_team extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  OC_team.init({
    image_path: DataTypes.STRING,
    name: DataTypes.STRING,
    role: DataTypes.STRING,
    email: DataTypes.STRING,
    tel: DataTypes.STRING,
    year: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'OC_team',
    timestamps: false
  });
  return OC_team;
};