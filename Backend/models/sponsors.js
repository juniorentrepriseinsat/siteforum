'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Sponsors extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Sponsors.init({
    logo_path: DataTypes.STRING,
    name: DataTypes.STRING,
    domain: DataTypes.STRING,
    type: DataTypes.STRING,
    pack: DataTypes.STRING,
    active: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Sponsors',
    timestamps: false
  });
  return Sponsors;
};