const express = require('express')
const router = express.Router();
const {Timeline_2019_element} = require('../models');

router.get('/', async (req, res) => {
    const elements = await Timeline_2019_element.findAll();

    res.json(elements);
});

module.exports = router;