const express = require('express')
const router = express.Router();
const {News} = require('../models');

router.get('/', async (req, res) => {
    const { page } = req.query;
    const limit = 5;
    const offset = (page-1)*limit;
    
    const news = await News.findAndCountAll({
        limit,
        offset
    })

    const response = {
        news: news.rows,
        totalPages: Math.ceil(news.count / limit)
    }

    res.json(response);

});

module.exports = router;