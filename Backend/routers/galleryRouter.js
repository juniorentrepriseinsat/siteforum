const express = require('express')
const router = express.Router();
const {Gallery_image} = require('../models');

router.get('/', async (req, res) => {
    const images = await Gallery_image.findAll();

    const copy = [...images];

    const result = [];

    for(var i=1;i<=4; i++)
    {
        var elementIndex = Math.floor(Math.random()*copy.length);
        result.push(copy[elementIndex]);
        copy.splice(elementIndex, 1);
    }

    
    res.json(result);


});

module.exports = router;