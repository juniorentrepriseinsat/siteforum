const express = require('express')
const router = express.Router();
const {Sponsors} = require('../models');

router.get('/', async (req, res) => {
    const sponsors = await Sponsors.findAll();
    res.json(sponsors);
});

module.exports = router;