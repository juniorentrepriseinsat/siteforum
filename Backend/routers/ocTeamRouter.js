const express = require('express')
const router = express.Router();
const {OC_team} = require('../models');

router.get('/', async (req, res) => {
    const team = await OC_team.findAll();

    res.json(team);
});

module.exports = router;