const express = require('express');
const app = express();
const { Sequelize } = require('sequelize');
const bodyParser = require('body-parser')
const cors = require('cors');


require('dotenv').config({path: "./utils/.env"})

const port = process.env.port;

const db = require('./models');

const articleRouter = require("./routers/articleRouter")
const galleryImageRouter = require("./routers/galleryRouter")
const newsRouter = require("./routers/newsRouter")
const ocTeamRouter = require("./routers/ocTeamRouter")
const sponsorsRouter = require("./routers/sponsorsRouter")
const timelineRouter = require("./routers/timelineRouter")


//parse information comming from client
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//allow cross origin access
app.use(cors());



//routers for each model
app.use('/api/article', articleRouter);
app.use('/api/gallery', galleryImageRouter);
app.use('/api/news', newsRouter);
app.use('/api/octeam', ocTeamRouter);
app.use('/api/sponsors', sponsorsRouter);
app.use('/api/timeline', timelineRouter);

//check if connection with database is successful and then start the server
(async () => {
    try {
        await db.sequelize.authenticate()
        console.log("connected to database successfully");
        app.listen(port, () => {
            console.log(`server is listening at http://localhost:${port}`);
        })
    }
    catch(error){
        console.error('Unable to connect to the database: ', error);
    }
})();




    

