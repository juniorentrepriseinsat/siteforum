'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Sponsors', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      logo_path: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      domain: {
        type: Sequelize.STRING
      },
      type: {
        type: Sequelize.STRING
      },
      pack: {
        type: Sequelize.STRING
      },
      active: {
        type: Sequelize.BOOLEAN
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Sponsors');
  }
};