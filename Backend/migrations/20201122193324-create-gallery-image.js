'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Gallery_images', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      path: {
        type: Sequelize.STRING
      },
      year: {
        type: Sequelize.STRING
      },
      active: {
        type: Sequelize.BOOLEAN
      },
  
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Gallery_images');
  }
};